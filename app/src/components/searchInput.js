import React from "react"

class searchInput extends React.Component
    {
    constructor(props){
        super(props)
        this.state = {
            searchValue: "",
            showPop: false,
            loading: false,
            loading2: false,
            dataset: {},
            dataset2: {},
            country: "",
            cityName: "",
            population: "",
            countryC: "",
            backImage: "",
            populationText: "",
            peopleText: "",
        }
        this.updateInput = this.updateInput.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        
    }
    
    //reformats the input to work well with the API and then sets the searchValue state
    updateInput(event){
        var temp = event.target.value;
        temp = temp.toLowerCase();
        temp = temp.replace(/  +/g, ' ');
        temp = temp.replace(" ","-");
        this.setState({searchValue : temp})
    }

    handleSubmit(){
        this.fetchCityData();
        this.fetchCityImage();
    }

    fetchCityData(){
        //Fetches the city related data via the geonames API: http://www.geonames.org/export/geonames-search.html
        //and sets the related states
        this.setState({loading: true})
        fetch("http://api.geonames.org/searchJSON?maxRows=3&name="+this.state.searchValue+"&cities15000&username=weknowit")
        .then(response => response.json())
        .then(data => {
            if(data.totalResultsCount === 0){

            }else{
                this.setState({
                    loading: false,
                    dataset: data,
                    country: data.geonames[0].countryName + " - ",
                    cityName: data.geonames[0].name,
                    population: data.geonames[0].population,
                    populationText: "Population",
                    peopleText: " People",
                    error: "",
                })
            }
        })
    }

    //Fetches the image related to the city via the teleport API: http://developers.teleport.org/api/
    //and sets the related states
    fetchCityImage(){
        this.setState({loading2: true})
        fetch("https://api.teleport.org/api/urban_areas/slug:"+this.state.searchValue+"/images/")
        .then(response => response.json())
        .then(data2 => {
            //console.log(data2.photos[0].image.web);
            if(data2.status === 404){
                console.log("Kunde inte ladda ned bild")
            }else{
                this.setState({
                    loading2: false,
                    dataset2: data2,
                    backImage: data2.photos[0].image.web,
                })
            }
        })
    }
    
    render(){
        const styleContain = {
            border: '0px',
            width: '100%',
            height: '50%',
            display: 'flex',
            justifyContent: "center",
            flexDirection: "column",
            alignItems: "center",
        }
        
        const styleImage = {
            position: "relative",
            display: this.state.loading2 ? "none" : "block",
            width: 400, 
            height:200,
            backgroundPosition: "top",
            objectFit: "cover",
            top: -20,
            zIndex: -1,
        }

        const styleObj = {
            color: 'black',
            width: '18em',
            height: '5em',
            display: 'flex',
            alignItems: 'center',
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            flexWrap: 'wrap',
            textAlign: 'center',
            margin: '0.4em',
        }

        const styleInputField = {
            width: "100%",
            height: 56,
            borderRadius: 4,
            border: "none",
            position: "relative",
            backgroundColor: "rgba(52, 152, 76, 0.4)",
            textAlign: "center",
            color: "white",
            fontSize: 20,
        }

        const styleBtn = {
            width: "100%",
            height: 56,
            zIndex: 999,
            borderRadius: 4,
            backgroundColor: "#34984c",
            lineHeight: "100%",
            fontWeight: 700,
            fontSize: 20,
            color: "white",
            textAlign: "center",
            border: "none",
            cursor: "pointer"
        }
        
        const styleObjPop = {
            color: 'black',
            width: '18em',
            height: 'contain',
            display: 'flex',
            alignItems: 'center',
            flexDirection: 'row',
            flexWrap: 'wrap',
            textAlign: 'center',
        }

        const stylePopulation = {
            width: '100%',
            height: 'fit-content',
        }

        const styleH3 = {
            fontSize: '1.3em',
            width: '100%',
            marginBottom: '2em',
            marginTop: '2em',
        }
        
        const styleSimpleText = {
            fontWeight: "normal",
            fontSize: "1.2em",
        }

        const stylePopulationText = {
            fontSize: '1.3em',
        }
    
        return(
            <div style={styleContain}>
                <img src={this.state.backImage} alt="city" style={styleImage} />
                <h1>CityPop</h1>
                <h3>Find the population of any city</h3>
                <div style={styleObj}>
                    <input type="text" style={styleInputField} placeholder="enter a city name" onChange={this.updateInput}></input>
                    <input type="submit" style={styleBtn} value="search" onClick={this.handleSubmit} ></input>
                </div>
                <div style={styleObjPop}>
                    <div style={stylePopulation}>
                        <h3 style={styleH3}>{this.state.country}{this.state.cityName}</h3>
                        <h3 style={styleSimpleText}>{this.state.populationText}</h3>
                        <h4 style={stylePopulationText}>{this.state.population.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",")}{this.state.peopleText}</h4>
                    </div>
                </div>
            </div>
            
        )
        
    }
}

export default searchInput