import React from "react"
import SearchByBox from "./searchByBox"
import ShowPopulation from "./showPopulation"
import SearchInput from "./searchInput"
import {
    BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams,
  useRouteMatch
  } from "react-router-dom";
class searchByCityBox extends React.Component{
    constructor(){
        super()
        this.state = {
            
        }
        this.handleClick = this.handleClick.bind(this)
    }

    handleClick(e) {
        e.preventDefault()
        
        
    }

    render(){
        
        const styleObj = {
            border: '2px solid black',
            color: 'black',
            width: '16em',
            height: '6em',
            display: 'flex',
            alignItems: 'center',
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            flexWrap: 'wrap',
            textAlign: 'center',
            margin: '0.4em',
        }
    
        const styleContain = {
            border: '0px',
            width: '100%',
            display: 'flex',
            justifyContent: "center",
        }

        const styleH2 = {
            fontSize: '0.8em',
        }


        return(
            
            <div>
                <div style={styleContain}>
                    <Link to="/searchCity">
                        <div style={styleObj}>
                            <h2 style={styleH2}>
                                {this.props.title1}
                            </h2>
                        </div>
                    </Link>
                </div>
            
                <div style={styleContain}>
                    <Link to="/searchCountry">
                        <div style={styleObj} onClick={this.handleClickCountry}>
                            <h2 style={styleH2}>
                                {this.props.title2}
                            </h2>
                        </div>
                    </Link>
                </div>
            </div> 
            
        )
    }
}


export default searchByCityBox