import React from "react"

class showPopulation extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            loading: true,
            searchValue: "",
            dataset: {}
        }
    }
    
    
    componentDidMount() {
        this.setState({loading: true})
        
        fetch("http://api.geonames.org/searchJSON?maxRows=5&country=fr&cities15000&username=weknowit")
        .then(response => response.json())
        .then(data => { 
            
            this.setState({
                loading: false,
                dataset: data,
            })
        })
    }

    

    render(){
        const name = this.state.loading ? "loading..." : this.state.dataset.geonames[0].name
        const population = this.state.loading ? "loading..." : this.state.dataset.geonames[0].population
        const styleObj = {
            color: 'black',
            width: '18em',
            height: '6em',
            display: 'flex',
            alignItems: 'center',
            flexDirection: 'row',
            flexWrap: 'wrap',
            textAlign: 'center',
        }
    
        const styleContain = {
            border: '0px',
            width: '100%',
            display: 'flex',
            justifyContent: "center"
        }

        const styleH3 = {
            fontSize: '0.8em',
            width: '100%',
            marginBottom: '4em',
        }
        const styleH5 = {
            fontSize: '0.6em',
            width: '100%',
            margin: '0.2em',
        }

        const stylePopulation = {
            border: '1px solid black',
            width: '100%',
            height: 'fit-content',
        }

        const stylePopulationText = {
            fontSize: '1.3em',
            margin: '0.5em',
        }
        
        return(
            <div style={styleObj}>
                <h3 style={styleH3}>{name}</h3>
                <div style={stylePopulation}>
                    <h5 style={styleH5}>Population</h5>
                    <h4 style={stylePopulationText}>{population}</h4>
                </div>
            </div>
        )
    }
}

export default showPopulation