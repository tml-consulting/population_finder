import React from "react"


class SearchByBox extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            loading: true,
            dataset: {}
        }
        this.componentDidMount = this.componentDidMount.bind(this)
    }

    componentDidMount() {
        this.setState({loading: true})
        fetch("http://api.geonames.org/searchJSON?maxRows=5&country=fr&cities15000&username=weknowit")
            .then(response => response.json())
            .then(data => {
                
                this.setState({
                    loading: false,
                    dataset: data
                })
            })
    }

    render(){
        const myData = [].concat(this.state.dataset)
    .sort((a, b) => a.geonames.population > b.geonames.population)
    .map((item, i) => 
        <div key={i}> {item.matchID} {item.timeM}{item.description}</div>
    );
        const country = this.state.loading ? "loading..." : this.state.dataset.geonames[0].countryName
        const city1 = this.state.loading ? "loading..." : this.state.dataset.geonames[0].name
        const city2 = this.state.loading ? "loading..." : this.state.dataset.geonames[1].name
        const city3 = this.state.loading ? "loading..." : this.state.dataset.geonames[2].name
        const city1pop = this.state.loading ? "loading..." : this.state.dataset.geonames[0].population
        const city2pop = this.state.loading ? "loading..." : this.state.dataset.geonames[1].population
        const city3pop = this.state.loading ? "loading..." : this.state.dataset.geonames[2].population
        
        const styleContain = {
            border: '0px',
            width: '100%',
            display: 'flex',
            justifyContent: "center",
            textAlign: "center"
        }

        const styleObj = {
            width: '14em',
            marginTop: '0px',
        }

        const styleUl = {
            width: '100%',
            margin: 'auto',
            padding: '0px',
        }
        const styleListItem = {
            listStyleType: 'none',
            textAlign: 'center',
            border: '1px solid black',
            padding: '0.7em',
            margin: '0.2em',
        }
        const styleH3 = {
            mariginTop: '0em',
            marginBottom: '2em',
        }

        return(
            <div style={styleContain}>
                <div style={styleObj}>
                    <h3 style={styleH3}>{country}</h3>
                    <ul style={styleUl}>
                        <li style={styleListItem}>{city1}<br/>Population: {city1pop}</li>
                        <li style={styleListItem}>{city2}</li>
                        <li style={styleListItem}>{city3}</li>
                    </ul>
                </div>
            </div>
            
        )
    }
}

/*const styleInput = {
            width: '66%',
            height: '3em',
            textAlign: 'center',
            position: 'absolute', left: '50%', top: '50%',
            transform: 'translate(-50%, -50%)',
        }*/

export default SearchByBox