import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import SearchInput from "./components/searchInput"
export default function App() {
  return (
    <Router>
        <Switch>
          <Route path="/searchCity">
            <SearchInput/>
          </Route>
          <Route path="/">
            <SearchInput/>
          </Route>
        </Switch>
    </Router>
  );
}
